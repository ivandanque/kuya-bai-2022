using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class KudlitManager : MonoBehaviour
{
    // Start is called before the first frame update
    public Button nextButton;
    public Button previousButton;
    public List<RectTransform> kudlitPages;
    private int pageIndex = 0;
    List<string> kudlitTitles = new List<string>() { "Kudlit", "Pamudpod or Krus-kudlit", "Let's review!"};

    public ScrollRect scrollRect;

    public TextMeshProUGUI nextButtonText;
    public TextMeshProUGUI kudlitTitleText;
    void Start()
    {
        nextButton.onClick.AddListener(NextContent);
        previousButton.onClick.AddListener(PreviousContent);
        SetContent();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PreviousContent();
        }
    }

    public void OpenLink()
    {
        Application.OpenURL("https://kuyabai.com/");
    }

    // Update is called once per frame
    void NextContent()
    {
        kudlitPages[pageIndex].gameObject.SetActive(false);
        
        if (pageIndex == kudlitPages.Count - 1)
        {
            GoHome();
        } else
        {
            pageIndex++;
            SetContent();   
        }
    }

    void PreviousContent()
    {
        kudlitPages[pageIndex].gameObject.SetActive(false);
        if (pageIndex == 0)
        {
            GoHome();
        } else
        {
            pageIndex--;
            SetContent();
        }
    }

    void SetContent()
    {
        scrollRect.content = kudlitPages[pageIndex];
        kudlitTitleText.text = kudlitTitles[pageIndex];
        kudlitPages[pageIndex].gameObject.SetActive(true);

        if (pageIndex == (kudlitPages.Count - 1))
        {
            nextButtonText.text = "Finish Lesson";

        } else
        {
            nextButtonText.text = "Next";
        }
    }

    public void GoHome()
    {
        LoadingManager.instance.LoadScene("MainMenuScreen");
    }
}
