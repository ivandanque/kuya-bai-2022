using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TracingButtonManager : MonoBehaviour
{
    public void LoadToQuizzes()
    {
        GManager.instance.CurrentProgress++;
        LoadingManager.instance.LoadScene("MultipleChoiceQuiz");
    }
}
