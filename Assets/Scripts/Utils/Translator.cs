using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Translator
{
    public string Translate(string rawText)
    {
        rawText = rawText.ToLower();

        //E and I use the same symbol
        rawText = rawText.Replace("e", "i");

        //O and U use the same symbol
        rawText = rawText.Replace("o", "u");

        // C and Q turns into K
        rawText = rawText.Replace("c", "k");
        rawText = rawText.Replace("q", "k");
        // F turns into P
        rawText = rawText.Replace("f", "p");
        rawText = rawText.Replace("v", "p");
        // X and Z turns into S
        rawText = rawText.Replace("x", "s");
        rawText = rawText.Replace("z", "s");
        //J turns into H
        rawText = rawText.Replace("j", "h");


        rawText = rawText.Replace("nga", "\u1705");
        rawText = rawText.Replace("ngi", "\u1705\u1712");
        rawText = rawText.Replace("ngu", "\u1705\u1713");
        rawText = rawText.Replace("ng", "\u1705\u1714");


        rawText = rawText.Replace("ka", "\u1703");
        rawText = rawText.Replace("ga", "\u1704");
        rawText = rawText.Replace("ta", "\u1706");
        rawText = rawText.Replace("da", "\u1707");
        rawText = rawText.Replace("na", "\u1708");
        rawText = rawText.Replace("pa", "\u1709");
        rawText = rawText.Replace("ba", "\u170A");
        rawText = rawText.Replace("ma", "\u170B");
        rawText = rawText.Replace("ya", "\u170C");
        rawText = rawText.Replace("ra", "\u1707");
        rawText = rawText.Replace("la", "\u170E");
        rawText = rawText.Replace("wa", "\u170F");
        rawText = rawText.Replace("sa", "\u1710");
        rawText = rawText.Replace("ha", "\u1711");

        rawText = rawText.Replace("ki", "\u1703\u1712");
        rawText = rawText.Replace("gi", "\u1704\u1712");
        rawText = rawText.Replace("ti", "\u1706\u1712");
        rawText = rawText.Replace("di", "\u1707\u1712");
        rawText = rawText.Replace("ni", "\u1708\u1712");
        rawText = rawText.Replace("pi", "\u1709\u1712");
        rawText = rawText.Replace("bi", "\u170A\u1712");
        rawText = rawText.Replace("mi", "\u170B\u1712");
        rawText = rawText.Replace("yi", "\u170C\u1712");
        rawText = rawText.Replace("ri", "\u1707\u1712");
        rawText = rawText.Replace("li", "\u170E\u1712");
        rawText = rawText.Replace("wi", "\u170F\u1712");
        rawText = rawText.Replace("si", "\u1710\u1712");
        rawText = rawText.Replace("hi", "\u1711\u1712");

        rawText = rawText.Replace("ku", "\u1703\u1713");
        rawText = rawText.Replace("gu", "\u1704\u1713");
        rawText = rawText.Replace("tu", "\u1706\u1713");
        rawText = rawText.Replace("du", "\u1707\u1713");
        rawText = rawText.Replace("nu", "\u1708\u1713");
        rawText = rawText.Replace("pu", "\u1709\u1713");
        rawText = rawText.Replace("bu", "\u170A\u1713");
        rawText = rawText.Replace("mu", "\u170B\u1713");
        rawText = rawText.Replace("yu", "\u170C\u1713");
        rawText = rawText.Replace("ru", "\u1707\u1713");
        rawText = rawText.Replace("lu", "\u170E\u1713");
        rawText = rawText.Replace("wu", "\u170F\u1713");
        rawText = rawText.Replace("su", "\u1710\u1713");
        rawText = rawText.Replace("hu", "\u1711\u1713");

        rawText = rawText.Replace("a", "\u1700");
        rawText = rawText.Replace("i", "\u1701");
        rawText = rawText.Replace("u", "\u1702");

        rawText = rawText.Replace("k", "\u1703\u1714");
        rawText = rawText.Replace("g", "\u1704\u1714");
        rawText = rawText.Replace("t", "\u1706\u1714");
        rawText = rawText.Replace("d", "\u1707\u1714");
        rawText = rawText.Replace("n", "\u1708\u1714");
        rawText = rawText.Replace("p", "\u1709\u1714");
        rawText = rawText.Replace("b", "\u170A\u1714");
        rawText = rawText.Replace("m", "\u170B\u1714");
        rawText = rawText.Replace("y", "\u170C\u1714");
        rawText = rawText.Replace("r", "\u1707\u1714");
        rawText = rawText.Replace("l", "\u170E\u1714");
        rawText = rawText.Replace("w", "\u170F\u1714");
        rawText = rawText.Replace("s", "\u1710\u1714");
        rawText = rawText.Replace("h", "\u1711\u1714");

        return rawText;
    }
}