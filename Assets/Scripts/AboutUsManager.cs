using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AboutUsManager : MonoBehaviour
{
    public Button homeButton;

    private void Start()
    {
        homeButton.onClick.AddListener(GoHome);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GoHome();
        }
    }

    public void GoHome()
    {
        LoadingManager.instance.LoadScene("MainMenuScreen");
    }
}
