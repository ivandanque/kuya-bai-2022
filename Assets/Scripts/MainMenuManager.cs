using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] private GameObject lessonList;
    [SerializeField] private GameObject gameList;
    public void LoadIntro()
    {
        if (GManager.instance)
        {
            GManager.instance.GameMode = GameMode.INTRO;
            LoadingManager.instance.LoadScene("IntroScreen");
        }
    }
    public void LoadKudlits()
    {
        if (GManager.instance)
        {
            GManager.instance.GameMode = GameMode.KUDLIT;
            LoadingManager.instance.LoadScene("KudlitScreen");
        }
    }
    public void LoadVowels()
    {
        if (GManager.instance)
        {
            GManager.instance.GameMode = GameMode.VOWEL;
            GManager.instance.HandleLoadedBaybayinList();
            LoadingManager.instance.LoadScene("LessonScreen");
        }
    }
    public void LoadConsonants(int consonantPart)
    {
        if (GManager.instance)
        {
            GManager.instance.GameMode = GameMode.CONSONANT;
            GManager.instance.HandleLoadedBaybayinList(consonantPart);
            LoadingManager.instance.LoadScene("LessonScreen");
        }
    }

    public void LoadAboutUs()
    {
        LoadingManager.instance.LoadScene("AboutUsScreen");
    }

    public void LoadTranslator()
    {
        LoadingManager.instance.LoadScene("TranslateScreen");
    }

    public void LoadTimeTrials()
    {
        if (GManager.instance)
        {
            GManager.instance.GameMode = GameMode.TIMETRIAL;
            LoadingManager.instance.LoadScene("TimeQuizScreen");
        }
    }

    public void SwapToGameList()
    {
        lessonList.SetActive(false);
        gameList.SetActive(true);
    }

    public void SwapToLessonList()
    {
        gameList.SetActive(false);
        lessonList.SetActive(true);
    }

    public void SignOut()
    {
        GManager.instance.SignOut();
    }
}
