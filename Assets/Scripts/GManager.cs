using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Firebase.Firestore;
using Firebase.Extensions;
using Google;
using IndieStudio.EnglishTracingBook.Game;
public enum GameMode
{
    INTRO,
    VOWEL,
    CONSONANT,
    KUDLIT,
    TIMETRIAL
}

public enum ThemeMode
{
    LIGHT,
    DARK
}
public class GManager : MonoBehaviour
{
    public static GManager instance;
    public List<BaybayinBase> vowelsBaybayin;
    public List<BaybayinBase> consonantsBaybayin;
    public List<BaybayinBase> kudlitBaybayin;


    private BaybayinBase selectedBaybayin;
    GameMode selectedMode = GameMode.CONSONANT;
    ThemeMode selectedTheme = ThemeMode.LIGHT;
    [SerializeField]
    private int maxBaybayinIndex = 0;
    [SerializeField]
    private int currentBaybayinIndex = 0;
    //GOOGle
    private GoogleSignInConfiguration configuration;
    public string webClientId = "68672479529-g1o2abe2mfhvcv81vonh99sldhjartv4.apps.googleusercontent.com";
    //FIREBASE 
    Firebase.FirebaseApp firebaseApp;
    Firebase.Auth.FirebaseAuth firebaseAuth;
    FirebaseFirestore firebaseStore;
    Firebase.Auth.FirebaseUser firebaseUser;
    
    [SerializeField] private bool trialComplete = false;
    [SerializeField] private bool trialStarted = false;
    [SerializeField]
    private float currentProgress = 0.0f;
    [SerializeField]
    private float maxProgress;
    bool signedIn = false;

    [SerializeField] private int score;
    [SerializeField] private int streakCounter;
    [SerializeField] private int highStreakScore;

    public AudioSource audioSource;
    void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        configuration = new GoogleSignInConfiguration
        {
            WebClientId = webClientId,
            RequestIdToken = true
        };

    }
    void Start()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                firebaseApp = Firebase.FirebaseApp.DefaultInstance;
                firebaseStore = FirebaseFirestore.DefaultInstance;
                firebaseAuth = Firebase.Auth.FirebaseAuth.DefaultInstance;

                firebaseAuth.StateChanged += AuthStateChanged;
                AuthStateChanged(this, null);

            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
        audioSource = GetComponent<AudioSource>();

        Application.targetFrameRate = Screen.currentResolution.refreshRate;
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (firebaseAuth.CurrentUser != firebaseUser)
        {
            signedIn = firebaseUser != firebaseAuth.CurrentUser && firebaseAuth.CurrentUser != null;
            if (!signedIn && firebaseUser != null)
            {
                Debug.Log("Signed out " + firebaseUser.UserId);
            }
            firebaseUser = firebaseAuth.CurrentUser;
            if (signedIn)
            {
                Debug.Log("Signed in " + firebaseUser.UserId);
                string displayName = firebaseUser.DisplayName ?? "";
                string emailAddress = firebaseUser.Email ?? "";
                string photoUrl = firebaseUser.PhotoUrl.ToString() ?? "";
            }
        }
    }
    
    public int Score
    {
        set { this.score = value; }
        get { return this.score; }
    }

    public int StreakCounter
    {
        set { this.streakCounter = value; }
        get { return this.streakCounter; }
    }

    public int HighStreakScore
    {
        set { this.highStreakScore = value; }
        get { return this.highStreakScore; }
    }

    public void ResetScore()
    {
        GManager.instance.Score = 0;
        GManager.instance.StreakCounter = 0;
        GManager.instance.HighStreakScore = 0;
    }

    public GameMode GameMode
    {
        set { selectedMode = value; }
        get { return selectedMode; }
    }
    public ThemeMode ThemeMode
    {
        set { selectedTheme = value; }
        get { return selectedTheme; }
    }
    public List<BaybayinBase> ConsonantsBaybayin
    {
        get { return consonantsBaybayin; }
    }

    public List<BaybayinBase> VowelsBaybayin
    {
        get { return vowelsBaybayin; }
    }

    public List<BaybayinBase> KudlitBaybayin
    {
        get { return kudlitBaybayin; }
    }

    public bool TrialComplete
    {
        set { trialComplete = value; }
        get { return trialComplete;  }
    }

    public bool TrialStarted
    {
        set { trialStarted = value; }
        get { return trialStarted; }
    }

    public void ResetTimeTrial()
    {
        this.trialStarted = false;
        this.trialComplete = false;
    }
    public int CurrentBaybayinIndex
    {
        set { currentBaybayinIndex = value; }
        get { return currentBaybayinIndex; }
    }

    public float CurrentProgress
    {
        set { currentProgress = value; }
        get { return currentProgress; }
    }
    public float MaxProgress
    {
        set { maxProgress = value; }
        get { return maxProgress; }
    }
    public int MaxBaybayinIndex
    {
        set { maxBaybayinIndex = value; }
        get { return maxBaybayinIndex; }
    }
    public List<BaybayinBase> GetBaybayinBaseList()
    {
        List<BaybayinBase> sortedBaybayinBase = new List<BaybayinBase>();
        switch (GameMode)
        {
            case GameMode.VOWEL:
                {
                    sortedBaybayinBase = GManager.instance.VowelsBaybayin;
                    break;
                }
            case GameMode.CONSONANT:
                {
                    sortedBaybayinBase = GManager.instance.ConsonantsBaybayin;
                    break;
                }
        }
        MaxBaybayinIndex = sortedBaybayinBase.Count;
        return sortedBaybayinBase;
    }
    public BaybayinBase SelectedBaybayinBase
    {
        set { selectedBaybayin = value; }
        get { return selectedBaybayin; }
    }
    public Firebase.FirebaseApp FirebaseApp
    {
        get { return firebaseApp; }
    }

    public FirebaseFirestore FirebaseStore
    {
        get { return firebaseStore; }
    }

    public Firebase.Auth.FirebaseAuth FirebaseAuth
    {
        get { return firebaseAuth; }
    }
    public Firebase.Auth.FirebaseUser FirebaseUser
    {
        get { return firebaseUser; }
    }
    public bool SignedIn
    {
        get { return signedIn; }
    }
    public void HandleLoadedBaybayinList()
    {
        switch (GameMode)
        {
            case GameMode.VOWEL:
                {
                    ShapesManager.shapesManagerReference = "VowelsManager";
                    SelectedBaybayinBase = vowelsBaybayin[currentBaybayinIndex];
                    MaxBaybayinIndex = vowelsBaybayin.Count;
                    maxProgress = 12.0f;
                    break;
                }
            case GameMode.CONSONANT:
                {
                    ShapesManager.shapesManagerReference = "ConsonantsManager";
                    SelectedBaybayinBase = consonantsBaybayin[currentBaybayinIndex];
                    break;
                }
            case GameMode.KUDLIT:
                {
                    ShapesManager.shapesManagerReference = "KudlitManager";
                    break;
                }
            default:
                {
                    Debug.Log("You shall not pass!");
                    break;
                }
        }
    }
    public void PlayBaybayinLetterSFX()
    {
        AudioClip clip = selectedBaybayin.baseAudio;
        Debug.Log(clip.name);
        audioSource.PlayOneShot(clip);
    }
    public void HandleLoadedBaybayinList(int consonantIndex)
    {
        if (GameMode == GameMode.CONSONANT)
        {
            switch (consonantIndex)
            {
                case 0:
                    {
                        currentBaybayinIndex = 0;
                        SelectedBaybayinBase = consonantsBaybayin[currentBaybayinIndex];
                        MaxBaybayinIndex = 3;
                        maxProgress = 12.0f;
                        //3 letter
                        break;
                    }
                case 1:
                    {
                        currentBaybayinIndex = 3;
                        SelectedBaybayinBase = consonantsBaybayin[currentBaybayinIndex];
                        //4
                        MaxBaybayinIndex = 7;
                        maxProgress = 16.0f;
                        break;
                    }
                case 2:
                    {
                        currentBaybayinIndex = 7;
                        SelectedBaybayinBase = consonantsBaybayin[currentBaybayinIndex];
                        MaxBaybayinIndex = 11;
                        maxProgress = 16.0f;
                        //4
                        break;
                    }
                case 3:
                    {
                        currentBaybayinIndex = 11;
                        SelectedBaybayinBase = consonantsBaybayin[currentBaybayinIndex];
                        MaxBaybayinIndex = consonantsBaybayin.Count;
                        maxProgress = 12.0f;
                        //3
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            ShapesManager.shapesManagerReference = "ConsonantsManager";
        }
        else
        {
            Debug.Log("Not a consonant");
        }
    }
    public void HandleShapeToLoad()
    {
        ShapesManager.Shape.selectedShapeID = currentBaybayinIndex;
    }
    public void LoadNextBaybayin()
    {
        GManager.instance.CurrentBaybayinIndex++;
        if (currentBaybayinIndex < maxBaybayinIndex)
        {
            HandleLoadedBaybayinList();
            LoadingManager.instance.LoadScene("LessonScreen");
            HandleShapeToLoad();
        }
        else if (currentBaybayinIndex >= maxBaybayinIndex)
        {
            ResetLoadedData();
            LoadingManager.instance.LoadScene("MainMenuScreen");
        }
        else
        {
            Debug.Log("Something went wrong");
            ResetLoadedData();
        }
    }
    private void ResetLoadedData()
    {
        selectedBaybayin = null;
        selectedMode = GameMode.INTRO;
        currentBaybayinIndex = 0;
        maxBaybayinIndex = 0;
        currentProgress = 0.0f;
    }
    public void SetFirestore()
    {

        DocumentReference docRef = firebaseStore.Collection("users").Document("alovelace");
        Dictionary<string, object> user = new Dictionary<string, object>
{
        { "First", "Ada" },
        { "Last", "Lovelace" },
        { "Born", 1815 },
};
        docRef.SetAsync(user).ContinueWithOnMainThread(task =>
        {
            Debug.Log("Added data to the alovelace document in the users collection.");
        });
    }

    public void CreateUser(string email, string password)
    {
        firebaseAuth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            // Firebase user has been created.
            firebaseUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                firebaseUser.DisplayName, firebaseUser.UserId);
        });
    }
    public void SignInWithEmail(string email, string password)
    {
        firebaseAuth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
         {
             if (task.IsCanceled)
             {
                 Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                 return;
             }
             if (task.IsFaulted)
             {
                 Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                 return;
             }

             // Firebase user has been created.
             firebaseUser = task.Result;
             Debug.LogFormat("Firebase user Logged In successfully: {0} ({1})",
                 firebaseUser.DisplayName, firebaseUser.UserId);
         });
    }
    public void SignInWithGoogle()
    {
        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(task =>
         {
             if (task.IsFaulted)
             {
                 using (IEnumerator<System.Exception> enumerator =
                         task.Exception.InnerExceptions.GetEnumerator())
                 {
                     if (enumerator.MoveNext())
                     {
                         GoogleSignIn.SignInException error =
                                 (GoogleSignIn.SignInException)enumerator.Current;
                         Debug.Log("Got Error: " + error.Status + " " + error.Message);
                     }
                     else
                     {
                         Debug.Log("Got Unexpected Exception?!?" + task.Exception);
                     }
                 }
             }
             else if (task.IsCanceled)
             {
                 Debug.Log("Canceled");
             }
             else
             {
                 Debug.Log("Welcome: " + task.Result.DisplayName + "!");
             }
         });
    }
    public void SignInWithFacebook()
    {

    }
    public void SignOut()
    {
        firebaseAuth.SignOut();
        LoadingManager.instance.LoadScene("SignIn");
    }
}
