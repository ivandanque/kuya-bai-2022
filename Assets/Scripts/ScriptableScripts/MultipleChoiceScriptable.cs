using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "KuyaBai/Holder/Games/MultipleChoice")]
public class MultipleChoiceScriptable : ScriptableObject
{
    public IllustrationsBase illustrationsBase;
    public string question;
    public List<string> choices;

}
