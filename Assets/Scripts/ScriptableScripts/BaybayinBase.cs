using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("KuyaBaiData/BaybayinBase"))]
public class BaybayinBase : ScriptableObject
{
    //Index 0- Black 1- White
    public List<Sprite> baseImage;
    public string baseName;
    public AudioClip baseAudio;
}