using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MEC;
public class LoadingManager : MonoBehaviour
{
    public static LoadingManager instance;

    public CanvasGroup canvasGroup;
    public enum FadeType
    {
        FADE_IN,
        FADE_OUT
    }
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    void OnDestroy()
    {
        if (this.GetInstanceID() == instance.GetInstanceID())
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
    }
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (canvasGroup != null)
        {
            canvasGroup.alpha = 1;
            Timing.RunCoroutine(_CanvasFade(FadeType.FADE_OUT, scene.buildIndex).CancelWith(gameObject));
        }
    }
    public void LoadScene(int index)
    {
        Timing.RunCoroutine(_LoadScene(index).CancelWith(gameObject));
    }
    public void LoadScene(string sceneName)
    {
        Timing.RunCoroutine(_LoadScene(sceneName).CancelWith(gameObject));
    }
    IEnumerator<float> _LoadScene(int index)
    {
        yield return Timing.WaitUntilDone(_CanvasFade(FadeType.FADE_IN, index).CancelWith(gameObject));
    }
    IEnumerator<float> _LoadScene(string sceneName)
    {
        yield return Timing.WaitUntilDone(_CanvasFade(FadeType.FADE_IN, sceneName).CancelWith(gameObject));
    }
    IEnumerator<float> _CanvasFade(FadeType fadeType, int sceneIndex)
    {
        float loadProgress = 0;


        canvasGroup.blocksRaycasts = true;
        if (fadeType == FadeType.FADE_IN)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneIndex);
            while (loadProgress < 1)
            {
                canvasGroup.blocksRaycasts = true;
                loadProgress += asyncLoad.progress;
                canvasGroup.alpha = ExtendedLerp.Smootherstep(canvasGroup.alpha, loadProgress, Time.fixedDeltaTime * 5f);
                yield return Timing.WaitForSeconds(0.02f);
            }
        }
        else
        {
            while (canvasGroup.alpha > 0)
            {
                canvasGroup.blocksRaycasts = true;
                float newValue = canvasGroup.alpha -= 0.1f;
                canvasGroup.alpha = ExtendedLerp.Smootherstep(canvasGroup.alpha, newValue, Time.fixedDeltaTime * 5f);
                yield return Timing.WaitForSeconds(0.02f);
            }
        }
        canvasGroup.blocksRaycasts = false;
    }
    IEnumerator<float> _CanvasFade(FadeType fadeType, string sceneName)
    {
        float loadProgress = 0;


        canvasGroup.blocksRaycasts = true;
        if (fadeType == FadeType.FADE_IN)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
            while (loadProgress < 1)
            {
                canvasGroup.blocksRaycasts = true;
                loadProgress += asyncLoad.progress;
                canvasGroup.alpha = ExtendedLerp.Smootherstep(canvasGroup.alpha, loadProgress, Time.fixedDeltaTime * 5f);
                yield return Timing.WaitForSeconds(0.02f);
            }
        }
        else
        {
            while (canvasGroup.alpha > 0)
            {
                canvasGroup.blocksRaycasts = true;
                float newValue = canvasGroup.alpha -= 0.1f;
                canvasGroup.alpha = ExtendedLerp.Smootherstep(canvasGroup.alpha, newValue, Time.fixedDeltaTime * 5f);
                yield return Timing.WaitForSeconds(0.02f);
            }
        }
        canvasGroup.blocksRaycasts = false;
    }
    public string GetActiveSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }
}
