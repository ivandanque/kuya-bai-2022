using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
public class MultipleChoiceManager : MonoBehaviour
{
    public Image questionImage;
    public TextMeshProUGUI questionText;
    //holder of questions
    public List<MultipleChoiceScriptable> vowelsQuestionData;
    public List<MultipleChoiceScriptable> consonantsQuestionData;
    public List<TextMeshProUGUI> choicesText;
    private BaybayinBase selectedBase;
    [SerializeField]
    private List<string> baseNames = new List<string>();

    public Sprite correctAnswerSprite;
    public Sprite wrongAnswerSprite;
    public Button nextButton;
    void Start()
    {
        nextButton.interactable = false;
        nextButton.onClick.AddListener(() => LoadNextLetter());
        //Load baybayin base
        //create two random shit for choices
        List<BaybayinBase> tempList = new List<BaybayinBase>(GManager.instance.VowelsBaybayin);
        foreach (BaybayinBase bases in tempList)
        {
            baseNames.Add(bases.baseName);
        }
        tempList = new List<BaybayinBase>(GManager.instance.ConsonantsBaybayin);
        foreach (BaybayinBase bases in tempList)
        {
            baseNames.Add(bases.baseName);
        }


        if (GManager.instance.SelectedBaybayinBase)
        {
            selectedBase = GManager.instance.SelectedBaybayinBase;
        }
        else
        {
            Debug.Log("No base");
        }
        SetupQuiz();
    }
    public void SetupQuiz()
    {
        Shuffler.Shuffle(choicesText);

        questionImage.sprite = selectedBase.baseImage[1];


        for (int x = 0; x < 3; x++)
        {
            if (x == 0)
            {
                choicesText[x].text = selectedBase.baseName;
                baseNames.Remove(selectedBase.baseName);
            }
            else
            {
                int randomIdx = Random.Range(0, baseNames.Count - 1);
                Shuffler.Shuffle(baseNames);
                choicesText[x].text = baseNames[randomIdx];
                baseNames.RemoveAt(randomIdx);
            }
        }
    }
    public void CheckAnswer(TextMeshProUGUI value)
    {
        nextButton.onClick.RemoveListener(LoadNextLetter);
        Image inputBox = value.transform.GetComponentInParent<Image>();
        if (value.text == selectedBase.baseName)
        {
            inputBox.sprite = correctAnswerSprite;
            nextButton.interactable = true;

        }
        else
        {
            Timing.RunCoroutine(_WrongAnswer(inputBox).CancelWith(gameObject));
        }
    }
    public void LoadNextLetter()
    {
        nextButton.interactable = false;
        nextButton.onClick.RemoveListener(LoadNextLetter);
        GManager.instance.CurrentProgress++;
        GManager.instance.LoadNextBaybayin();
        // Timing.RunCoroutine(_LoadNextLetter(inputImage).CancelWith(gameObject));
    }
    IEnumerator<float> _LoadNextLetter(Image choiceImage)
    {
        yield return Timing.WaitForSeconds(1f);
        GManager.instance.LoadNextBaybayin();
    }
    IEnumerator<float> _WrongAnswer(Image choiceImage)
    {
        Sprite prevSprite = choiceImage.sprite;
        choiceImage.sprite = wrongAnswerSprite;
        yield return Timing.WaitForSeconds(0.25f);
        choiceImage.sprite = prevSprite;
    }
}
