using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using IndieStudio.EnglishTracingBook.Game;
public class LessonManager : MonoBehaviour
{
    public List<IllustrationsBase> vowelWordsBase;
    public List<IllustrationsBase> consonantWordsBase;
    public IllustrationsBase selectedBase;
    [SerializeField]
    Image lessonLetterImageBG;
    [SerializeField]
    Image lessonLetterImage;
    [SerializeField]
    TextMeshProUGUI lessonLetterText;
    [SerializeField]
    GameObject lessonLetter;
    [SerializeField]
    GameObject lessonWordsContainer;
    [SerializeField]
    Image secondContainerImage;
    public Button nextButton;

    public IllustrationsBase specialIllusBaseRA;
    void Start()
    {
        SetupBaybayinLetter();
    }
    private void SetupBaybayinLetter()
    {
        lessonLetterText.text = GManager.instance.SelectedBaybayinBase.baseName.ToLower();
        lessonLetterImage.sprite = GManager.instance.SelectedBaybayinBase.baseImage[1];
        switch (GManager.instance.GameMode)
        {
            case GameMode.VOWEL:
                {
                    selectedBase = vowelWordsBase[GManager.instance.CurrentBaybayinIndex];
                    break;
                }
            case GameMode.CONSONANT:
                {
                    if (GManager.instance.CurrentBaybayinIndex == 2)
                    {
                        int randomizeIllus = Random.Range(0, 1);
                        if (randomizeIllus == 0)
                        {
                            selectedBase = consonantWordsBase[GManager.instance.CurrentBaybayinIndex];
                        }
                        else
                        {
                            selectedBase = specialIllusBaseRA;
                        }
                    }
                    else
                    {
                        selectedBase = consonantWordsBase[GManager.instance.CurrentBaybayinIndex];
                    }

                    break;
                }
        }
        nextButton.onClick.AddListener(LoadNextInfo);

    }

    public void PlayLetterSound()
    {
        // Disabled for now because of the lack of official voiceovers
        // GManager.instance.PlayBaybayinLetterSFX();
    }
    private void LoadNextInfo()
    {

        nextButton.onClick.RemoveListener(LoadNextInfo);
        nextButton.onClick.AddListener(LoadNextLetter);
        lessonLetter.SetActive(false);
        lessonWordsContainer.SetActive(true);
        lessonLetterImage.transform.gameObject.SetActive(false);
        lessonLetterImageBG.sprite = selectedBase.illustrationSprite;
        secondContainerImage.sprite = selectedBase.illustrationTranslationImage;
        GManager.instance.CurrentProgress++;
        //hide first info
        //show next infor
    }


    private void LoadNextLetter()
    {
        //TODO check if increment now
        // GManager.instance.CurrentBaybayinIndex++;

        //
        GManager.instance.CurrentProgress++;
        GManager.instance.HandleShapeToLoad();
        LoadingManager.instance.LoadScene("Game");
        //LOAD to TRacing or quiz

        // LoadingManager.instance.LoadScene("MultipleChoiceQuiz");
        // if (GManager.instance.CurrentBaybayinIndex < GManager.instance.MaxBaybayinIndex)
        // {
        //     nextButton.onClick.RemoveListener(LoadNextLetter);
        //     lessonLetter.SetActive(true);
        //     lessonWordsContainer.SetActive(false);
        //     SetupBaybayinLetter();
        // }
        // else
        // {
        //     Debug.Log("We are done");
        //     //Show done?
        // }
    }
}
