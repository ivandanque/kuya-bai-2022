using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MEC;

public class ProgressHandler : MonoBehaviour
{
    [SerializeField]
    Image progressBar;
    float percentage;
    void Start()
    {
        progressBar = transform.GetChild(1).GetComponent<Image>();
        progressBar.fillAmount = (GManager.instance.CurrentProgress / GManager.instance.MaxProgress) - 0.025f;
    }
    void FixedUpdate()
    {
        percentage = GManager.instance.CurrentProgress / GManager.instance.MaxProgress;
        // progressBar.fillAmount = ExtendedLerp.Smootherstep(progressBar.fillAmount, percentage, Time.fixedDeltaTime * 10f);
        progressBar.fillAmount = percentage;
    }
}
