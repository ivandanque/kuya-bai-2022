using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using IndieStudio.EnglishTracingBook.Game;
public class TimeTrialsManager : MonoBehaviour
{

    public Image questionImage;
    public TextMeshProUGUI questionText;
    
    public Button choice1;
    public Button choice2;
    public Button choice3;
    //holder of questions

    public List<TextMeshProUGUI> choicesText;
    private BaybayinBase selectedBase;
    [SerializeField]
    private List<string> baseNames = new List<string>();
    
    [SerializeField]
    private List<BaybayinBase> wholeList;

    public Sprite correctAnswerSprite;
    public Sprite wrongAnswerSprite;
    public Button nextButton;

    float trialTime = 60.0f;

    [SerializeField] private GameObject choiceGroup;
    [SerializeField] private GameObject scoreGroup;

    [SerializeField] private TextMeshProUGUI displayScoreText;
    [SerializeField] private TextMeshProUGUI displayStreakText;

    AudioClip previousCorrectSFX;
    public AudioClip correctSFX;

    public TextMeshProUGUI nextButtonText;
    void Start()
    {
        previousCorrectSFX = AudioSources.instance.correctSFX;
        AudioSources.instance.correctSFX = correctSFX;
        if (GManager.instance.TrialStarted == false)
        {
            GManager.instance.TrialStarted = true;
            GManager.instance.MaxProgress = trialTime;
            GManager.instance.CurrentProgress = trialTime;
        }
                
        
        nextButton.onClick.AddListener(() => GoHome());
        nextButton.gameObject.SetActive(false);
        //Load baybayin base
        
        
        foreach (BaybayinBase bases in GManager.instance.VowelsBaybayin)
        {
            wholeList.Add(bases);
            baseNames.Add(bases.baseName);
        }

        foreach (BaybayinBase bases in GManager.instance.ConsonantsBaybayin)
        {
            wholeList.Add(bases);
            baseNames.Add(bases.baseName);
        }

        foreach (BaybayinBase bases in GManager.instance.KudlitBaybayin)
        {
            wholeList.Add(bases);
            baseNames.Add(bases.baseName);
        }

        selectedBase = wholeList[Random.Range(0, wholeList.Count)];
        
        SetupQuiz();
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            GoHome();
        }
        if (GManager.instance.CurrentProgress <= 0.0f)
        {
            choiceGroup.SetActive(false);
            
            displayScoreText.text = $"Score\n{GManager.instance.Score}";
            displayStreakText.text = $"Highest Combo Streak: {GManager.instance.HighStreakScore}";
            scoreGroup.SetActive(true);

            nextButtonText.text = "Exit Time Trial";
            nextButton.gameObject.SetActive(true);
        } else
        {
            GManager.instance.CurrentProgress -= 1.0f * Time.deltaTime;
        }
        
        
    }
    public void SetupQuiz()
    {
        Shuffler.Shuffle(choicesText);

        questionImage.sprite = selectedBase.baseImage[1];


        for (int x = 0; x < 3; x++)
        {
            if (x == 0)
            {
                choicesText[x].text = selectedBase.baseName;
                baseNames.Remove(selectedBase.baseName);
            }
            else
            {
                int randomIdx = Random.Range(0, baseNames.Count - 1);
                Shuffler.Shuffle(baseNames);
                choicesText[x].text = baseNames[randomIdx];
                baseNames.RemoveAt(randomIdx);
            }
        }
    }
    public void CheckAnswer(TextMeshProUGUI value)
    {
        AudioSources.instance.PlayCorrectSFX();
        Image inputBox = value.transform.GetComponentInParent<Image>();
        if (value.text == selectedBase.baseName)
        {
            inputBox.sprite = correctAnswerSprite;
            GManager.instance.StreakCounter++;
            if (GManager.instance.StreakCounter >= 10)
            {
                GManager.instance.Score += 8;
            }
            if (GManager.instance.StreakCounter >= 6)
            {
                GManager.instance.Score += 4;
            }
            if (GManager.instance.StreakCounter >= 4)
            {
                GManager.instance.Score += 2;
            }
            else
            {
                GManager.instance.Score += 1;
            }

            if (GManager.instance.StreakCounter > GManager.instance.HighStreakScore)
            {
                GManager.instance.HighStreakScore = GManager.instance.StreakCounter;
            }

            Debug.Log($"Score is added. The score is {GManager.instance.Score}");
            LoadingManager.instance.LoadScene(LoadingManager.instance.GetActiveSceneName());
        }
        else
        {
            AudioSources.instance.PlayWrongSFX();
            GManager.instance.StreakCounter = 0;
            Timing.RunCoroutine(_WrongAnswer(inputBox).CancelWith(gameObject));
        }
    }
    

    public void GoHome()
    {
        
        AudioSources.instance.correctSFX = previousCorrectSFX;
        LoadingManager.instance.LoadScene("MainMenuScreen");
        scoreGroup.SetActive(false);
        GManager.instance.ResetTimeTrial();
        GManager.instance.ResetScore();
    }
    //IEnumerator<float> _LoadNextLetter(Image choiceImage)
    //{
    //    yield return Timing.WaitForSeconds(1f);
    //    // GManager.instance.LoadNextBaybayin();
    //}
    IEnumerator<float> _WrongAnswer(Image choiceImage)
    {
        Sprite prevSprite = choiceImage.sprite;
        choiceImage.sprite = wrongAnswerSprite;
        yield return Timing.WaitForSeconds(0.25f);
        choiceImage.sprite = prevSprite;
    }
}
