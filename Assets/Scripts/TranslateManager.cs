using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TranslateManager : MonoBehaviour
{
    Translator translator = new Translator();
    // Start is called before the first frame update

    public TMP_InputField input;
    public InputField output;
    
    [SerializeField]
    string inputText;
    [SerializeField]
    string outputText;

    public Button homeButton;
    public Button copyButton;

    private void Start()
    {
        homeButton.onClick.AddListener(GoHome);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GoHome();
        }
    }

    public void GoHome()
    {
        LoadingManager.instance.LoadScene("MainMenuScreen");
    }


    public void getText()
    {
        inputText = input.text.ToLower();
        outputText = translator.Translate(inputText);
    }

    public void setText()
    {
        output.text = outputText;
    }

    public void getBaybayinString()
    {
        GUIUtility.systemCopyBuffer = outputText;
    }

}
