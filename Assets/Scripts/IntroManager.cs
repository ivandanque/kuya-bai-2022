using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class IntroManager : MonoBehaviour
{
    public ScrollRect scrollRect;
    public List<RectTransform> contentsRect = new List<RectTransform>();
    public TextMeshProUGUI contentTitle;
    public TextMeshProUGUI nextButtonText;
    public Button nextButton;
    public Button previousButton;
    List<string> contentTitles = new List<string>() { "What is Baybayin?", "Letters of Baybayin", "Vowels", "Vowels" };
    private int currentContentIndex = 0;
    void Start()
    {
        nextButton.onClick.AddListener(NextContent);
        previousButton.onClick.AddListener(PreviousContent);
        SetContent();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PreviousContent();
        }
    }

    public void OpenLink()
    {
        Application.OpenURL("https://kuyabai.com/");
    }

    void NextContent()
    {
        contentsRect[currentContentIndex].gameObject.SetActive(false);

        if (currentContentIndex == contentsRect.Count - 1)
        {
            GoHome();
        }
        else
        {
            currentContentIndex++;
            SetContent();
        }
    }

    void PreviousContent()
    {
        contentsRect[currentContentIndex].gameObject.SetActive(false);
        if (currentContentIndex == 0)
        {
            GoHome();
        }
        else
        {
            currentContentIndex--;
            SetContent();
        }
    }

    void SetContent()
    {
        scrollRect.content = contentsRect[currentContentIndex];
        contentTitle.text = contentTitles[currentContentIndex];
        contentsRect[currentContentIndex].gameObject.SetActive(true);

        if (currentContentIndex == (contentsRect.Count - 1))
        {
            nextButtonText.text = "Finish Lesson";

        }
        else
        {
            nextButtonText.text = "Next";
        }
    }

    public void GoHome()
    {
        LoadingManager.instance.LoadScene("MainMenuScreen");
    }
}
