using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using TMPro;

public class SplashScreenManager : MonoBehaviour
{
    public List<GameObject> sponsors = new List<GameObject>();
    public TextMeshProUGUI versionText;
    void Start()
    {
        Timing.RunCoroutine(_ShowSponsors().CancelWith(gameObject));
    }
    private void Awake()
    {
        versionText.text = Application.version;
    }

    IEnumerator<float> _ShowSponsors()
    {
        for (int x = 0; x < sponsors.Count; x++)
        {

            if (x == 0)
            {
                yield return Timing.WaitForSeconds(1.0f);
            }
            sponsors[x].SetActive(true);
            yield return Timing.WaitForSeconds(1.5f);
            sponsors[x].SetActive(false);
        }
        if (sponsors.Count == 0)
        {
            yield return Timing.WaitForSeconds(1.5f);
        }

        //  yield return Timing.WaitUntilTrue(() => GameManager.instance.SignedIn);
        LoadingManager.instance.LoadScene("MainMenuScreen");
        // if (GameManager.instance.FirebaseUser != null)
        // {
        //     LoadingManager.instance.LoadScene("MainMenuScreen");
        // }
        // else
        // {
        //     LoadingManager.instance.LoadScene("SignIn");
        // }
    }
}
