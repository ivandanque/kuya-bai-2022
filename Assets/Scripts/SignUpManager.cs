using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System.Text.RegularExpressions;

public class SignUpManager : MonoBehaviour
{
    private string email;
    private string password;
    public TextMeshProUGUI emailError;
    public TextMeshProUGUI passError;
    private bool isEmailValid = false;
    private bool isPasswordValid = false;
    public Button signupButton;
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            LoadingManager.instance.LoadScene("MainMenuScreen");
        }

        if (isEmailValid && isPasswordValid)
        {
            signupButton.interactable = true;
        }
        else
        {
            signupButton.interactable = false;
        }
    }
    public void EmailHandler(TMP_InputField value)
    {
        bool isEmail = Regex.IsMatch(value.text,
        @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                       @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                       @".)+))([a-zA-Z]{2,6}|[0-9]{1,3})(\]?)$"
        , RegexOptions.IgnoreCase);
        if (isEmail)
        {
            email = value.text;
            isEmailValid = true;
        }
        else
        {
            isEmailValid = false;
        }
    }
    public void PasswordHandler(TMP_InputField value)
    {
        string tempString = value.text;

        Regex hasNumber = new Regex(@"[0-9]+");
        Regex hasUpperChar = new Regex(@"[A-Z]+");
        Regex hasMinimum8Chars = new Regex(@".{8,}");

        bool isValidated = hasNumber.IsMatch(tempString) && hasUpperChar.IsMatch(tempString) && hasMinimum8Chars.IsMatch(tempString);
        if (isValidated)
        {
            password = tempString;
            isPasswordValid = true;
        }
        else
        {
            isPasswordValid = false;
        }

    }
    public void SignUpFirebase()
    {
        GManager.instance.CreateUser(email, password);
    }
    public void SignInWithEmail()
    {
        GManager.instance.SignInWithEmail(email, password);
    }
    public void SignInWithGoogle()
    {
        GManager.instance.SignInWithGoogle();
    }
}
